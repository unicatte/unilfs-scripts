#!/bin/bash
(
if [ -z "${1}" ]; then
  >&2 echo "error: need to specify package to build"
  exit 1
fi
exec_path="$(dirname "$(readlink -f "${0}")")"
if ( ! [ -d "${exec_path}" ] ) || ( ! [ -f "${exec_path}/${0}" ] ); then
  >&2 echo "error: failed to set initial execution directory"
  exit 1
fi
unilfs_local_share="${HOME}/.local/share/unilfs" &&
builddir="${unilfs_local_share}/builddir" &&
sources_dir="${LFS}/sources" &&
if ! [ -f "xtoolchain/${1}" ]; then
  >&2 echo "error: specified package source ${1} doesn't exist"
  exit 1
fi
. "xtoolchain/${1}"
if [ -z "${pkgname}" ] || [ -z "${pkgver}" ]; then
  >&2 echo "error: package source lacks necessary vars"
  exit 1
fi

case "${builddir}/${pkgname}" in
        "/${pkgname}" | "${builddir}/")
                >&2 echo "MISCONFIGURED VARIABLES - RISK OF REMOVING ESSENTIAL SYSTEM FILES. EXITING NOW"
                exit 1
                ;;
esac &&

if [ -d "${builddir}/${pkgname}" ]; then
        rm -rvf "${builddir}/${pkgname}"
fi &&
mkdir -pv "${builddir}/${pkgname}" &&

cd "${builddir}/${pkgname}" &&
build
)
