#!/bin/bash
if [ ${EUID} -ne 0 ]; then
	>&2 echo "Not running as root."
	exit 1
fi &&
source functions.bash &&
source lfs.env &&
# Default to /mnt/lfs if $LFS var not set
LFS="${LFS:-"/mnt/lfs"}" &&

chown -R root:root "${LFS}"/{usr,lib{,32},var,etc,bin,sbin} &&
(chown -R root:root "${LFS}/tools" || true) &&
case $(uname -m) in
  x86_64)
    chown -R root:root "${LFS}"/lib64 ||
	(
		error_code=$? 
		>&2 echo "Case instructions failed."
		report_error ${error_code}
		exit ${error_code}
	)
    ;;
esac &&

mkdir -pv "$LFS"/{dev,proc,sys,run} &&
mount -v --bind /dev "$LFS/dev" &&
mount -v --bind /dev/pts "$LFS/dev/pts" &&
mount -vt proc proc "$LFS/proc" &&
mount -vt sysfs sysfs "$LFS/sys" &&
mount -vt tmpfs tmpfs "$LFS/run" &&

if [ -h "$LFS/dev/shm" ]; then
  (cd "$LFS/dev"; mkdir "$(readlink shm)") ||
	(
		error_code=$? 
		>&2 echo "Failed creating shm."
		report_error ${error_code}
		exit ${error_code}
	)
else
  mount -vt tmpfs -o nosuid,nodev tmpfs "$LFS/dev/shm" ||
	(
		error_code=$? 
		>&2 echo "Failed creating shm."
		report_error ${error_code}
		exit ${error_code}
	)
fi &&

chroot "$LFS" /usr/bin/env -i   \
    HOME=/root                  \
    TERM="${TERM}"              \
    PS1='(lfs chroot) \u:\w\$ ' \
    PATH=/usr/bin:/usr/sbin     \
    MAKEFLAGS="-j$(nproc)"      \
    TESTSUITEFLAGS="-j$(nproc)" \
    /bin/bash --login

mountpoint -q "$LFS/dev/shm" && umount "$LFS/dev/shm" &&
umount "${LFS}"/dev/pts &&
umount "${LFS}"/{sys,proc,run,dev}
