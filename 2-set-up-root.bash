#!/bin/bash
# TODO: This error handling literally doesn't work. Rewrite to use if instead.
if [ ${EUID} -ne 0 ]; then
	>&2 echo "Not running as root."
	exit 1
fi &&
source functions.bash &&
source lfs.env &&
# Default to /mnt/lfs if $LFS var not set
LFS="${LFS:-"/mnt/lfs"}" &&

[ -d "${LFS}/sources" ] || mkdir -v "${LFS}/sources" 2> /dev/null &&
chmod -v a+wt "${LFS}/sources" &&

# download sources

grep -v '^#' packages.list | while read line; do
	[ -f "${LFS}/sources/$(basename "${line}")" ] ||
	(
		cd "${LFS}/sources" &&
		[ -f "$(basename "${line}")" ] ||
		(
			echo "Downloading $(basename "${line}")..." &&
			curl -LO "${line}" 
		)
	) ||
	(
		error_code=$? 
		echo "Archive download failed"
		report_error ${error_code}
		exit ${error_code}
	)
done &&

# TODO: redownload archives that fail the check

cp -v md5sums "${LFS}/sources/"

pushd "${LFS}/sources" &&
	md5sum -c md5sums ||
	(
		error_code=$? 
		echo "Archives failed to validate"
		report_error ${error_code}
		exit ${error_code}
	) &&
popd &&

chown root:root "${LFS}/sources/"* &&

mkdir -pv ${LFS}/{etc,var} ${LFS}/usr/{bin,lib,sbin} &&

for i in bin lib sbin; do
	ln -sv usr/${i} ${LFS}/${i} ||
	(
		error_code=$? 
		echo "Failed to create directory layout"
		report_error ${error_code}
		exit ${error_code}
	)
done &&

case $(uname -m) in
	x86_64)
		mkdir -pv $LFS/lib64 ||
		(
			error_code=$?
			echo "Failed to create directory layout"
			report_error ${error_code}
			exit ${error_code}
		)
		;;
esac &&

mkdir -pv "${LFS}/usr/lib32" &&
ln -sv usr/lib32 "${LFS}/lib32" &&
mkdir -pv ${LFS}/tools &&

(
	groupadd lfs ||
	# if exit code 9 (group already exists), continue anyways
	[ "${?}" = 9 ]
) &&
(
	useradd -s /bin/bash -g lfs -m -k /dev/null lfs ||
	# if exit code 9 (user already exists), continue anyways
	[ "${?}" = 9 ]
)

chown -v lfs "${LFS}"/{usr{,/*},lib,var,etc,bin,sbin,tools} &&
case $(uname -m) in
	x86_64)
		chown -v lfs "${LFS}"/lib64 ||
		(
			error_code=$?
			echo "Case instructions failed"
			report_error ${error_code}
			exit ${error_code}
		)
		;;
esac &&
chown -v lfs "${LFS}/lib32" &&

echo "Initial setup finished! Set the lfs user password with" &&
echo "passwd lfs" &&
echo "switch to the lfs user" &&
echo "su - lfs" &&
echo "and run the next script."
