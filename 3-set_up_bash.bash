#!/bin/bash
. functions.bash
. lfs.env
[ ! -e /etc/bash.bashrc ] || sudo mv -v /etc/bash.bashrc /etc/bash.bashrc.NOUSE
[ ! -e "${HOME}/.bash_profile" ] || mv -v "${HOME}/.bash_profile" "${HOME}/.bash_profile.NOUSE"
cp .bash_profile "${HOME}"
[ ! -e "${HOME}/.bashrc" ] || mv -v "${HOME}/.bashrc" "${HOME}/.bashrc.NOUSE"
cp .bashrc "${HOME}"
source "${HOME}/.bash_profile"
