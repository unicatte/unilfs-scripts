#!/bin/bash
. functions.bash
. lfs.env
grep -v '^#' packages.list | while read line; do
	[ -f "$(basename "${line}")" ] ||
	(
		cd "${LFS}/sources" &&
		([ -f "$(basename "${line}")" ] ||
		curl -LO "${line}")
	) ||
	(
		error_code=$?
		echo "While loop instructions failed"
		report_error ${error_code}
		exit ${error_code}
	)
done
